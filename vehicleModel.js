const mongoose = require('mongoose');

// create a schema for vehicle objects for mongodb interaction.
var vehicleSchema = new mongoose.Schema({
    class: Number,
    roi: Number,
    time: String},
    {collection : 'Demo1'}
);

// create model class for vehicles for mongodb interaction.
var Vehicle = mongoose.model('Vehicle', vehicleSchema);

module.exports = Vehicle;