// jQuery
$(document).ready(function(){
  
  $('form').submit(function(e) {
    e.preventDefault();

    $.ajax({
      url: "/",
      type: 'POST',
      dataType: 'json',
      data: $('form').serialize(),
    }).done(res => {
      // location.reload();
      console.log(res);
      $('.removable').remove();
      // ------ Create results: all should have class="removable" ------
      // create results title.
      var results_title = $('<h1 id="results_title" class="removable">Query Results:</h1>');
      $('form').after(results_title);
      // create image.
      var roi_img = $('<img src="images/' + res.im + '"' + ' alt="" id="roi_img" class="removable">');
      $('#results_title').after(roi_img);
      // create classes title.
      var classes_title = $('<h2 id="classes_title" class="removable">Classes:</h2>')
      $('#count-results').append(classes_title);
      // create classes list.
      var classes_list = $('<ul></ul>');
      $("#classes_title").append(classes_list)
      classes_list.attr('class', 'removable');
      for(var i = 0; i < res.classes.length; i++) {
        var li = $('<li class="removable">' + res.classes[i] + "," + '</li>')
        classes_list.append(li);
      }
      // create time title.
      var times_title = $('<h2 id="times_title" class="removable">Time interval:</h2>')
      // create times list.
      $('#classes_title').after(times_title);
      var times_list = $('<ul></ul>');
      $('#times_title').append(times_list);
      times_list.attr('class', 'removable');
      times_list.append('<li class="removable">' + 'from ' + res.times[0] + '</li>');
      times_list.append('<li class="removable">' + ', to ' + res.times[1] + '</li>');
      // create num_vehs result.
      $('#times_title').after('<h2 class="removable"> Number of Vehicles: ' + res.num_vehs + '</h2>');
      // create iframe for graph.
      var graph = $('<iframe id="graph" class="removable" width="900" height="800" frameborder="0" scrolling="no" src="//plotly.com/~ababababab/0.embed"></iframe>');
      $('#count-results').after(graph);
      // cannot get graph to reload with updated data, must click Search button again.
    });
  });
  
  $('#r_all').change(function() {
    $('.roi').prop("checked", this.checked);
  });

  $('#v_all').change(function() {
    $('.vehicle').prop("checked", this.checked);
  });

  $('#clear').on('click', function() {
    $('.removable').hide();
  });

});
  