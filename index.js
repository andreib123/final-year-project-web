var express = require('express');
var bodyParser = require('body-parser');
var mainController = require('./controllers/mainController');

var app = express();
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('./public'));

// set up template engine.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// call controllers.
mainController(app);

// listen to port.
app.listen(3000);
console.log('listening to port 3000');