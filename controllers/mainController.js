var plotly = require('plotly')("ababababab", "VQtDnZ4E7M1x4oqLSzNo"); // must change usr/key to new one every 100 graphs for free version.
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://andrei:admin@cluster0-cnuh2.mongodb.net/FinalYearProject?retryWrites=true&w=majority', {useNewUrlParser: true});

const Vehicle = require('../vehicleModel'); // get the Vehicle Mongoose model.

module.exports = function(app) {
    // setup request handlers for this controllers route (1st arg).
    app.get('/', function(req, res) {
        res.render('main_view', {im: ".jpeg", classes: [], times: [], num_vehs: 0}) // compiles main_view.ejs template into HTML output.
    });

    app.post('/', function(req, res) {
        // console.log(req.body.bottom);
        // create vars needed for a mongodb query from the url's JSON formatted query string.
        // req.query is a JSON object of the query string.
        var to_send = {};
        var vclasses_db = [];
        var rois_db = [];
        var roi_img = "";
        var vclasses_ui = [];
        var times = [];
        

        if(req.body.pickup) {
            vclasses_db.push(0);
            vclasses_ui.push("pickup");
        }
        if(req.body.sedan) {
            vclasses_db.push(1);
            vclasses_ui.push("sedan");
        }
        if(req.body.suv) {
            vclasses_db.push(2);
            vclasses_ui.push("SUV");
        }
        if(req.body.van) {
            vclasses_db.push(3);
            vclasses_ui.push("van");
        }
        to_send.classes = vclasses_ui;
        
        if(req.body.top) {
            rois_db.push(0);
            roi_img += "top";
        }
        if(req.body.bottom) {
            rois_db.push(1);
            roi_img += "bot";
        }
        if(req.body.left) {
            rois_db.push(2);
            roi_img += "left";
        }
        if(req.body.right) {
            rois_db.push(3);
            roi_img += "right";
        }

        roi_img += ".jpeg";
        to_send.im = roi_img;

        var start_time = "";
        var end_time = "";
        if(req.body.start) {
            start_time = req.body.start;
            times.push(start_time);
        }
        else {
            start_time = "1999-01-01T00:00"; // query from first time entry (w/o additional database query to find time entry).
            times.push("first");
        }
        if(req.body.end) {
            end_time = req.body.end;
            times.push(end_time);
        }
        else {
            end_time = "2999-01-01T00:00"; // query to last time entry (w/o additional database query to find time entry).
            times.push("last");
        }
        to_send.times = times;

        // get data from mongodb.
        // $<operator> are Mongoose operators.
        var cnt = 0;
        Vehicle.count(
            { $and:
                [
                    {class: {$in: vclasses_db}},
                    {roi: {$in: rois_db}},
                    {time: {$gte: start_time, $lte: end_time}}
                ]
            }, function(err, data) {
            if(err)
                throw err;
                
                to_send.num_vehs = data;
        });

        Vehicle.find(
                { $and:
                    [
                        {class: {$in: vclasses_db}},
                        {roi: {$in: rois_db}},
                        {time: {$gte: start_time, $lte: end_time}}
                    ]
                }, function(err, data) {
            if(err)
                throw err;
            
            // console.log(data);

            var traces = []; // array of plotly trace objects.

            if(vclasses_db.includes(0)) {
                traces.push({name: "pickup", x: [], y: [], type: "scatter"});
            }
            if(vclasses_db.includes(1)) {
                traces.push({name: "sedan", x: [], y: [], type: "scatter"});
            }
            if(vclasses_db.includes(2)) {
                traces.push({name: "suv", x: [], y: [], type: "scatter"});
            }
            if(vclasses_db.includes(3)) {
                traces.push({name: "van", x: [], y: [], type: "scatter"});
            }
            
            // add xaxis and yaxis properties to traces beginning with 2nd trace (1st trace is defualt).
            for(var i = 1; i < traces.length; i++) {
                var j = i+1;
                traces[i].xaxis = "x" + j.toString();
                traces[i].yaxis = "y" + j.toString();
            }

            if(data != undefined) {
                data.forEach(function(vehicle) {
                    var class_name = get_class_name(vehicle.class)
                    traces.forEach((trace) => {
                        if(trace.name === class_name) {
                            let time_str = new String(vehicle.time)
                            time_str = time_str.replace('T', ' ');
                            time_str += ":00";
                            trace.x.push(time_str);
                            trace.y.push(1);
                        }
                    });
                });

                new_traces = [];
                for(var i = 0; i < traces.length; i++) {
                    new_traces.push(removeObjDuplicates(traces[i]))
                    new_traces[i].name = traces[i].name;
                    new_traces[i].type = traces[i].type;
                }

                // plot data.
                var layout = {
                    legend: {traceorder: "reversed"},
                    yaxis: {title: "Number of occurences"},
                    xaxis: {title: "Time"}
                };
                layout.title = "";
                new_traces.forEach(function(trace) {
                    layout.title += trace.name + " "
                });
                layout.title += "ENTERED FROM ";
                rois_db.forEach(function(roi_id) {
                    layout.title += get_roi_name(roi_id) + " ";
                })
                var graphOptions = {layout: layout, filename: "stacked-subplots-time-series", fileopt: "overwrite"};

                // plot
                plotly.plot(new_traces, graphOptions, function (err, msg) {
                    if(err) console.log(err);
                        console.log(msg);
                });
            }
            console.log(to_send)
            res.send(to_send);
        });
    });
};

function removeObjDuplicates(obj) {

    const { x, y } = obj;
  
    const occurrences = {};
  
    for (let i = 0; i < x.length; i++) {
      if (occurrences[x[i]]) {
        occurrences[x[i]] += y[i]
      }
      else {
        occurrences[x[i]] = y[i];
      }
    }
  
    return {
      x: Object.keys(occurrences),
      y: Object.values(occurrences),
    }
  }

function get_class_name(class_id) {
    switch(class_id) {
        case 0: return "pickup";
        case 1: return "sedan";
        case 2: return "suv";
        case 3: return "van";
    }
    return "";
}

function get_roi_name(roi_id) {
    switch(roi_id) {
        case 0: return "top";
        case 1: return "bottom";
        case 2: return "left";
        case 3: return "right";
    }
    return "";
}